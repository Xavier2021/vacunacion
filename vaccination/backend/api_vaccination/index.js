const config = require('./src/config/config')

const server = require('./server')
const init = async () => {
  try {
    const fastify = await server()
    const host = '0.0.0.0'

    await fastify.listen(config.PORT, host)

    fastify.log.info(`fastify üöÄ  server listening on ${config.PORT}`)
  } catch (error) {
    throw error
  }
}

module.exports = init()
