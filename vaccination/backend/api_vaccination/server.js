const fastify = require('fastify')({
  logger: {
    prettyPrint: true
  }
})
const cors = require('fastify-cors')
const formBody = require('fastify-formbody')
const helmet = require('fastify-helmet')
const routes = require('fastify-routes')
const path = require('path')
const pointOfView = require('point-of-view')
const redis = require('./src/integrations/redis.connection')
const mongo = require('./src/integrations/mongodb.connection')

// integrations connects
mongo.connect()
redis.connect()
// Registers
fastify.register(helmet, {
  hidePoweredBy: {setTo: 'PHP 4.0'}
})
fastify.register(cors)
fastify.register(routes)
fastify.register(formBody)



// Declare a route
// require('./src/auth/router', { prefix: '/api-v1/' })(fastify)
require('./src/users/router', {prefix: '/api-v1/'})(fastify)
require('./src/vaccination/router', {prefix: '/api-v1/'})(fastify)
// require('./src/rols/route', { prefix: '/api-v1/' })(fastify)
// require('./src/department/route', { prefix: '/api-v1/' })(fastify)
fastify.get('/', (req, reply) => {
  reply.send({msg: 'Up Backend'})
})
const createServer = async () => {
  // Run the servr!
  fastify.setErrorHandler((error, req, reply) => {
    req.log.error(error.toString())
    reply.send({error})
  })

  return fastify
}

module.exports = createServer
