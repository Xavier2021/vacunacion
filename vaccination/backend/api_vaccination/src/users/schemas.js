const codesSchema = require('../schemas/basicSchemas')
const {formatBasic} = require('../schemas/service.schemas')

/** **---------| @Model user parms |--------****
                  Model user parms
****------------------------**** */

const baseModel = {
  headers: () => {
    return {
      token: {
        type: 'string'
      }
    }
  },
  params: () => {
    return {
      id: {
        type: 'string'
      }
    }
  },
  user: () => {
    return {
      ci: {
        type: 'number',
        minLength: 10,
        maxLength: 10
      },
      name: {
        type: 'string'
      },
      last_name: {
        type: 'string'
      },
      email: {
        type: 'string',
        format: 'email'

      },
      date_of_brith_user: {
        type: 'string'
      },
      password: {
        type: 'string'
      },
      address: {
        type: 'string'
      },
      phone: {
        type: 'string'
      },
      vaccination: {
        type: 'string'
      },
      is_admin: {
        type: 'boolean'
      },
      status: {
        type: 'boolean'
      }
    }
  }
}

const createSchema = {
  body: formatBasic(
    baseModel.user(),
    ['ci', 'name', 'email', 'last_name'],
    'filter'
  ),
  response: codesSchema
}

const updateSchema = {

  params: formatBasic(baseModel.params(), ['id'], 'filter'),
  response: codesSchema
}
const readSchema = {
  params: formatBasic(baseModel.params(), ['id'], 'filter'),
  response: codesSchema
}

const deleteSchema = {
  body: formatBasic(
    baseModel.user(), ['ci'], 'filter'
  ),
  params: formatBasic(baseModel.params(), ['id'], 'filter'),
  response: codesSchema
}



module.exports = {
  createSchema,
  updateSchema,
  readSchema,
  deleteSchema

}
