const serviceAuth = require('../auth/auth')
const ErrorBasic = require('../hooks/errors.basic')
const userModel = require('./model')

module.exports = class userDAO {
  static async create(obj) {
    try {
      obj.password = await serviceAuth.encriptPassword(obj.ci)


      const userSave = await userModel.create(obj)
      userSave.password = null
      return {
        ok: true,
        http: 201,
        item: userSave,
        message: 'Create success'
      }
    } catch (error) {
      console.log(error, 'dao')
      return ErrorBasic.basic(error)
    }
  }

  static async update(obj, id) {
    try {
      obj.password = obj.password ? await serviceAuth.encriptPassword(obj.password) : null
      const userUpdate = await userModel.findByIdAndUpdate(id, obj, {new: true})
      console.log(userUpdate, 'updte')
      return {
        ok: userUpdate ? true : false,
        http: userUpdate ? 200 : 400,
        items: [userUpdate],
        message: userUpdate ? `Update success with id: ${id}` : `Error update fail`
      }
    } catch (error) {
      return ErrorBasic.basic(error)
    }
  }
  static async list() {
    try {
      const userList = await userModel.find().populate('vaccination_id')
      return {
        ok: userList ? true : false,
        http: userList ? 200 : 400,
        items: userList,
        message: `List success with `
      }
    } catch (error) {
      return ErrorBasic.basic(error)
    }
  }
  static async read(_id) {
    try {

      const userRead = await userModel.findById(_id)
      return {
        ok: userRead ? true : false,
        http: userRead ? 200 : 400,
        items: [userRead],
        message: `Read success with id: ${_id}`
      }
    } catch (error) {
      console.log(error)
      return ErrorBasic.basic(error)
    }
  }
  static async delete(_id) {
    try {
      const userDelete = await userModel.findOneAndDelete({_id})
      console.log(userDelete, 'delete')
      return {
        ok: userDelete ? true : false,
        http: userDelete ? 200 : 400,
        item: userDelete,
        message: userDelete ? `Delete success with id: ${_id}` : `Error delete fail`
      }
    } catch (error) {
      console.log(error)
      return ErrorBasic.basic(error)
    }
  }
  static async findId(id) {
    try {

      const user = await userModel.findById(id).lean()
      return user
    } catch (error) {
      return ErrorBasic.basic(error)
    }
  }
  static async findCi(_id) {
    try {
      console.log(_id, 'findOne')
      const userRead = await userModel.findOne({_id}).lean()
      return userRead
    } catch (error) {

      return ErrorBasic.basic(error)
    }
  }

  static async deleteOneVaccination(_id, idVaccination) {
    try {
      console.log(_id, 'findOne')
      const userRead = await userModel.findByIdAndUpdate(_id, {$pull: {vaccination_id: [idVaccination]}})
      return userRead
    } catch (error) {

      return ErrorBasic.basic(error)
    }
  }
  static async save(_id, obj) {
    try {
      console.log(_id, obj)
      const add = await userModel.findByIdAndUpdate(_id, obj, {new: true})
      return add
    } catch (error) {
      console.log(error)
      return ErrorBasic.basic(error)
    }
  }
  static async search(obj) {
    try {

      const {name, to, from} = obj
      let search

      if (name && to && from) {
        search = {
          path: 'vaccination_id',
          match: {
            name: name, vaccination_date: {$gt: from, $lt: to}
          }

        }

      }

      else if (to && from) {

        search = {
          path: 'vaccination_id',

          match: {
            vaccination_date: {$gt: from, $lt: to}
          }

        }
      }

      else {
        search = {
          path: 'vaccination_id',
          match: {
            $or: [
              {name: obj.name},
              {vaccination_date: {$gt: from, $lt: to}}
            ]

          }
        }
      }
      console.log(search)
      let searchResults = await userModel
        .find().populate(search)
        .sort({createdAt: obj.order ? -1 : 1})
        .limit(Number(obj.limit))
        .exec()

      searchResults = searchResults.filter(v => v.vaccination_id && v.vaccination_id.length > 0)
      return searchResults
    } catch (error) {}
  }
}
