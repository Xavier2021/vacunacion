const DAO = require('./dao')
const ErrorBasic = require('../hooks/errors.basic')
const {deleteVaccination} = require('../vaccination/dao')
module.exports = class Controller {
  /* CRUD */



  static async create(req, reply) {
    try {

      const userCreate = await DAO.create(req.body)
      return reply.status(userCreate.http).send(userCreate)
    } catch (error) {
      console.log(error)
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async update(req, reply) {
    try {
      console.log(req.body,'body')
      const userUpdate = await DAO.update(req.body, req.params.id)
      return reply.status(userUpdate.http).send(userUpdate)

    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async list(req, reply) {
    try {
      const userList = await DAO.list()
      return reply.status(userList.http).send(userList)
    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async read(req, reply) {
    try {
      const {id} = req.params
      const userRead = await DAO.read(id)
      return reply.status(userRead.http).send(userRead)
    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async delete(req, reply) {
    try {
      const {id} = req.params

      const userDelete = await DAO.delete(id)
      if (userDelete && userDelete.vaccination_id && userDelete.vaccination_id.length > 0) {

        userDelete.vaccination_id.forEach(element => {
          deleteVaccination(element)
        })

      }
      return reply.status(userRead.http).send(userRead)
    } catch (error) {
      console.log(error)
      const err = ErrorBasic.error500(error)
      return err
    }
  }

  static async search(req, reply) {
    try {
      const query = req.query
      const obj = {
        name: query.name ? query.name : null,
        from: query.from ? new Date(query.from) : null,
        to: query.to ? new Date(query.to) : null,
        order: !!(query.order && query.order === 'true'),
        limit: query.limit ? query.limit : null
      }
      const searchVaccination = await DAO.search(obj)

      reply.status(200).send({
        message: 'vaccination search success',
        ok: true,
        items: searchVaccination,
        http: 200
      })
    } catch (error) {
      const err = error500(error)
      return reply.status(err.http).send(err)
    }
  }

}
