const userController = require('./controller')
const {createSchema, updateSchema, deleteSchema, readSchema} = require('./schemas')
//const globalHooks = require('../hooks/global.hooks')

module.exports = (fastify, passport) => {

  fastify.post('/user', {schema: createSchema}, userController.create)
  fastify.put('/user/:id', userController.update)
  fastify.get('/users', userController.list)
  fastify.get('/user/:id', {schema: readSchema}, userController.read)
  fastify.delete('/user/:id', {schema: deleteSchema}, userController.delete)
  fastify.get('/users/search', userController.search)
}
