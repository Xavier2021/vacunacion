const mongoose = require('mongoose')

const schema = new mongoose.Schema(
  {
    ci: {
      type: Number,
      maxLength: 10,
      unique: true
    },
    name: {
      type: String,
      trim: true,
      require: true
    },
    last_name: {
      type: String,
      require: true,
      trim: true
    },
    email: {
      type: String,
      require: true,
      unique: true,
      trim: true
    },
    password: {
      type: String,
      trim: true
    },
    date_of_birth_user: {
      type: String,
      trim: true
    },
    address: {
      type: String,
      trim: true
    },
    phone: {
      type: String,
      trim: true
    },
    is_admin: {
      type: Boolean,
      default: false
    },
    vaccination: {
      type: String,
      trim: true
    },
    vaccination_id: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Vaccination'
    }]
  },

  {
    timestamps: true
  }
)

module.exports = mongoose.model('Users', schema, 'users')
