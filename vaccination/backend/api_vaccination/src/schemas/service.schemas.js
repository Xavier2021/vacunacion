const {filterKeysService} = require('../hooks/global.hooks')

module.exports = class ServiceSchema {
  /** **---------| @format Schema base |--------****
                     format Schema base
    ****------------------------**** */

  static formatBasic(model, array, option, additional) {
    let properties
    let required = []

    const options = {
      all: () => {
        required = array
        properties = model
      },
      resto: () => {
        required = array
        properties = model
      },
      filter: () => {
        required = array
        properties = filterKeysService(model, array)
      },
      none: () => {
        required = []
        properties = model
      }
    }
    options[option]()
    const body = {
      tpe: 'object',
      required,
      properties,
      additionalProperties: additional || false
    }
    return body
  }
}
