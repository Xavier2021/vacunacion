const codesSchema = {
  201: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      http: {type: 'number'}
    },
    additionalProperties: false
  },
  400: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      http: {type: 'number'}
    },
    additionalProperties: false
  },
  401: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      http: {type: 'number'},
      intens: {type: 'number'}
    },
    additionalProperties: false
  },
  403: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      http: {type: 'number'}
    },
    additionalProperties: false
  },
  422: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      http: {type: 'number'}
    },
    additionalProperties: false
  },
  409: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      items: {type: 'array'},
      item: {type: 'array'},
      http: {type: 'number'}
    },
    additionalProperties: false
  },
  500: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      items: {type: 'array'},
      item: {type: 'array'},
      http: {type: 'number'}
    },
    additionalProperties: false
  },
  200: {
    type: 'object',
    properties: {
      ok: {type: 'boolean'},
      message: {type: 'string'},
      items: {type: 'array'},
      item: {type: 'array'},
      http: {type: 'number'},
      token: {type: 'string'}
    },
    additionalProperties: false
  }
}

module.exports = codesSchema
