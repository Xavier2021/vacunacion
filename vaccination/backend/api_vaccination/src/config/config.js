const dotenv = require('dotenv')

dotenv.config()

const config = {
  MONGO_DATABASE: process.env.MONGO_DATABASE || 'users',
  MONGO_PORT: process.env.MONGO_PORT_DB || 27017,
  MONGO_HOST: process.env.MONGO_HOST || 'mongo',
  MONGO_USER: process.env.MONGO_USER || 'root',
  MONGO_PASSWORD: process.env.MONGO_PASSWORD || 'rootpassword',
  REDIS_HOST: process.env.REDIS_HOST || 'redis',
  REDIS_PORT: process.env.REDIS_PORT || 6379,
  REDIS_PASSWORD: process.env.REDIS_PASSWORD || 'mypasswd',
  PORT: process.env.PORT || 3001,
  SECRET_KEY: process.env.SECRET_KEY || 'AAAAB3NzaC1yc2EAAAADAQABAAABgQDhg/A3gYMiKprR38VsXLRiSYN8kxyjA0vpoDMWGoA3dQ1RpjP/MMqKgppsZRMHEbDn796pmQ0Md5GCFfM9CWvE0XQvIX12Ep3IHjn8Etsff1M3aCtohE+FExTUyUA6s62HUKCSMipOUXFg2j0uhJnIC1JciEhu9Q8y24e7Z68AmPUazNE6I0/baPQ4QQ2O6g/1+YC4r1PqkQ6P7DYisBxKdiDtf3vV3h7+dbVo1waETcG+eAJTrWLHora6zx8nBTE00WeVXJiEXUph2WgCjQgvTKysT954mOwx7ZuQB1i0U1xuIHG5YAi+lTJP0/sQNeqsHMNF+qDHhp4ikXtC46K4PBllxLmKz38C62WTxJa3Wv+9iwBX0Tc42e0vAze42+SyY+jHq71z7WQgh8u/4JFob0UteYUXy3e88TnzoxuV6YHO7IAhUkNPq2TNvd80oAgEbXyNu9RDwKBMoHl35vFbd1E1WRNvIm6metv3Ydf/M/gtsFmRZaM+GYXBaM@@/',
  HOST_SERVER: process.env.HOST_SERVER || 'http://localhost',
  FRONT_URL: process.env.FRONT_URL || '/',
  VERSION_API: process.env.VERSION || '/api-v1/'
}

module.exports = config
