const config = require('../config/config')
const Redis = require('ioredis')
const redis = new Redis({
  port: config.REDIS_PORT, // Redis port
  host: config.REDIS_HOST,
  password: config.REDIS_PASSWORD
})

module.exports = class RedisConnection {
  static connect() {
    try {
      redis.on('connect', function () {
        console.log('Redis connection')
      })
      // client.end();
      redis.on('error', err => {

      })
    } catch (error) {
      return false
    }
  };

  static async get(key) {
    try {
      const red = await redis.get(key)
      return red
    } catch (error) {
      return false
    }
  }

  static async set(key, val, time) {
    try {
      time = time ? Number(time) * 60 + 7 : 60 * 60 * 60 + 7
      const set = await redis.set(key, va)
      await redis.expire(key, time)
      return set
    } catch (error) {
      return false
    }
  }

  static async delete(key) {
    try {
      const del = await redis.del(key)
      return del
    } catch (error) {
      return false
    }
  }
}
