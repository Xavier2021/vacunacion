const config = require('../config/config')
const mongoose = require('mongoose')

module.exports = class DB {
  static connect() {
    const dbConnect = `mongodb://${config.MONGO_HOST}/${config.MONGO_DATABASE}`
    mongoose.connect(dbConnect, {
      promiseLibrary: global.Promise,
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    })
    console.log(dbConnect, 'docker db')
  }
}
