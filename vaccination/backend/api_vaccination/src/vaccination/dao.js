const serviceAuth = require('../auth/auth')
const ErrorBasic = require('../hooks/errors.basic')
const vaccinationModel = require('./model')
const DAOUser = require('../users/dao')
const mongoose = require('mongoose')
module.exports = class vaccinationDAO {
  static async create(obj, id) {
    try {
      const user = await DAOUser.findId(id)
      if (!user) {
        return {
          ok: false,
          http: 400,
          message: 'User id not found'
        }

      }

      obj = Object.assign(obj, {user_id: user._id})
      const vaccinationSave = await vaccinationModel.create(obj)
      console.log(vaccinationSave, 'save')
      let vaccination = user.vaccination_id && user.vaccination_id.length > 0 ? user.vaccination_id : [];
      vaccination.push(vaccinationSave)
      console.log(vaccination, '-------')
      await DAOUser.save(user._id, {vaccination_id: vaccination})

      return {
        ok: true,
        http: 201,
        item: vaccinationSave,
        message: 'Create success'
      }
    } catch (error) {
      console.log(error, 'dao')
      return ErrorBasic.basic(error)
    }
  }

  static async update(obj, user_id, _id) {
    try {
      const vaccinationUpdate = await vaccinationModel.findOneAndUpdate({_id, user_id: mongoose.Types.ObjectId(user_id)}, obj, {new: true})

      return {
        ok: vaccinationUpdate ? true : false,
        http: vaccinationUpdate ? 200 : 400,
        items: [vaccinationUpdate],
        message: vaccinationUpdate ? `Update success with id: ${_id}` : `Error update fail`
      }
    } catch (error) {
      return ErrorBasic.basic(error)
    }
  }
  static async list() {
    try {
      const vaccinationList = await vaccinationModel.find()
      return {
        ok: vaccinationList ? true : false,
        http: vaccinationList ? 200 : 400,
        items: vaccinationList,
        message: `List success with `
      }
    } catch (error) {
      return ErrorBasic.basic(error)
    }

  }

  static async listId(_id) {
    try {
      const vaccinationList = await vaccinationModel.find({user_id: mongoose.Types.ObjectId(_id)})
      return {
        ok: vaccinationList ? true : false,
        http: vaccinationList ? 200 : 400,
        items: vaccinationList,
        message: `List success with `
      }
    } catch (error) {
      return ErrorBasic.basic(error)
    }
  }
  static async read(user_id, _id) {
    try {

      const vaccinationRead = await vaccinationModel.findOne({_id, user_id: mongoose.Types.ObjectId(user_id)})
      return {
        ok: vaccinationRead ? true : false,
        http: vaccinationRead ? 200 : 400,
        items: [vaccinationRead],
        message: `Read success with id: ${_id}`
      }
    } catch (error) {
      console.log(error)
      return ErrorBasic.basic(error)
    }
  }
  static async delete(user_id, _id) {
    try {
      const user = await DAOUser.findCi(user_id)
      if (!user) {
        return {
          ok: false,
          http: 400,
          message: `Error delete fail user not found`
        }

      }
      console.log(user, 'usersssssss')
      const vaccinationDelete = await vaccinationModel.findByIdAndDelete(_id)
      console.log(vaccinationDelete, '----delete')
      vaccinationDelete ? await DAOUser.deleteOneVaccination(user_id, _id) : null
      return {
        ok: vaccinationDelete ? true : false,
        http: vaccinationDelete ? 200 : 400,
        items: [vaccinationDelete],
        message: vaccinationDelete ? `Delete success with id: ${_id}` : `Error delete fail`
      }
    } catch (error) {
      console.log(error)
      return ErrorBasic.basic(error)
    }
  }

  static async deleteVaccination(id) {
    try {
      const vaccinationDelete = await vaccinationModel.findById(id)
      return vaccinationDelete

    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }

  static async search(obj) {
    try {

      console.log(obj, 'objectoss')
      const {name, to, from} = obj
      let search

      if (name && to && from) {
        search = {name: name, vaccination_date: {$gt: from, $lt: to}}
      }

      else if (to && from) {

        search = {vaccination_date: {$gt: from, $lt: to}}
      }

      else {
        search = {
          $or: [
            {name: obj.name},
            {vaccination_date: {$gt: from, $lt: to}}
          ]
        }
      }
      console.log(search, '---sssssss-')
      const searchResults = await vaccinationModel
        .find(search)
        .sort({createdAt: obj.order ? -1 : 1})
        .limit(Number(obj.limit))
      return searchResults
    } catch (error) {}
  }
}
