const vaccinationController = require('./controller')
const {createSchema, updateSchema, deleteSchema, readSchema} = require('./schemas')
//const globalHooks = require('../hooks/global.hooks')

module.exports = (fastify, passport) => {

  fastify.post('/vaccination/:id', {schema: createSchema}, vaccinationController.create)
  fastify.put('/vaccination/:id/:idUser', {schema: updateSchema}, vaccinationController.update)
  fastify.get('/vaccinations', vaccinationController.list)


  fastify.get('/vaccination/:id/:idUser', {schema: readSchema}, vaccinationController.read)
  fastify.delete('/vaccination/:id/:idUser', {schema: deleteSchema}, vaccinationController.delete)
  fastify.get('/vaccination/search', vaccinationController.search)

}
