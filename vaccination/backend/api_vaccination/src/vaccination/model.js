const mongoose = require('mongoose')

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      enum: ['Sputnik', 'AstraZeneca', 'Pfizer', 'Jhonson&Jhonson'],
      require: true

    },
    vaccination_date: {
      type: Date,
      trim: true,
      require: true
    },
    number_doses: {
      type: Number,
      require: true

    },
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  },

  {
    timestamps: true
  }
)

module.exports = mongoose.model('Vaccination', schema, 'vaccination')
