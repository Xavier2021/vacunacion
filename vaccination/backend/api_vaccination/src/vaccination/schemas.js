const codesSchema = require('../schemas/basicSchemas')
const {formatBasic} = require('../schemas/service.schemas')

/** **---------| @Model vaccination parms |--------****
                  Model vaccination parms
****------------------------**** */

const baseModel = {
  headers: () => {
    return {
      token: {
        type: 'string'
      }
    }
  },
  params: () => {
    return {
      id: {
        type: 'string'
      },
      idUser: {
        type: 'string'
      }
    }
  },
  vaccination: () => {
    return {
      name: {
        type: 'string',
        enum: ['Sputnik', 'AstraZeneca', 'Pfizer', 'Jhonson&Jhonson']
      },
      vaccination_date: {
        type: 'string'
      },
      number_doses: {
        type: 'number'
      }
    }
  }
}

const createSchema = {
  body: formatBasic(
    baseModel.vaccination(),
    ['name', 'vaccination_date', 'number_doses'],
    'filter'
  ),
  params: formatBasic(
    baseModel.params(),
    ['id'],
    'filter'
  ),
  response: codesSchema
}

const updateSchema = {
  body: formatBasic(
    baseModel.vaccination(), [], 'none'
  ),
  params: formatBasic(baseModel.params(), ['id', 'idUser'], 'filter'),
  response: codesSchema
}
const readSchema = {
  params: formatBasic(baseModel.params(), ['id', 'idUser'], 'filter'),
  response: codesSchema
}

const deleteSchema = {
  body: formatBasic(
    baseModel.vaccination(), ['ci'], 'filter'
  ),
  params: formatBasic(baseModel.params(), ['id', 'idUser'], 'filter'),
  response: codesSchema
}



module.exports = {
  createSchema,
  updateSchema,
  readSchema,
  deleteSchema

}
