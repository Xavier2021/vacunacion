const DAO = require('./dao')
const ErrorBasic = require('../hooks/errors.basic')
module.exports = class Controller {
  /* CRUD */

  static async create(req, reply) {
    try {
      const {id} = req.params
      const vaccinationCreate = await DAO.create(req.body, id)
      return reply.status(vaccinationCreate.http).send(vaccinationCreate)
    } catch (error) {
      console.log(error)
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async update(req, reply) {
    try {
      const {idUser, id} = req.params
      const vaccinationUpdate = await DAO.update(req.body, idUser, id)
      return reply.status(vaccinationUpdate.http).send(vaccinationUpdate)

    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async list(req, reply) {
    try {
      const vaccinationList = await DAO.list()
      return reply.status(vaccinationList.http).send(vaccinationList)
    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async listID(req, reply) {
    try {
      const vaccinationList = await DAO.listId(req.params.id)
      return reply.status(vaccinationList.http).send(vaccinationList)
    } catch (error) {
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async read(req, reply) {
    try {
      const {idUser, id} = req.params

      const vaccinationRead = await DAO.read(idUser, id)
      return reply.status(vaccinationRead.http).send(vaccinationRead)
    } catch (error) {
      console.log(error)
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async delete(req, reply) {
    try {
      const {idUser, id} = req.params



      const vaccinationDelete = await DAO.delete(idUser, id)
      return reply.status(vaccinationDelete.http).send(vaccinationDelete)
    } catch (error) {
      console.log(error)
      const err = ErrorBasic.error500(error)
      return err
    }
  }
  static async search(req, reply) {
    try {
      const query = req.query
      const obj = {
        name: query.name ? query.name : null,
        from: query.from ? new Date(query.from) : null,
        to: query.to ? new Date(query.to) : null,
        order: !!(query.order && query.order === 'true'),
        limit: query.limit ? query.limit : null
      }
      const searchVaccination = await DAO.search(obj)

      reply.status(200).send({
        message: 'vaccination search success',
        ok: true,
        items: searchVaccination,
        http: 200
      })
    } catch (error) {
      const err = error500(error)
      return reply.status(err.http).send(err)
    }
  }
}
