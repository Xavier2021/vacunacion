const config = require('../config/config')
const jwt = require('jsonwebtoken')

const bcrypt = require('bcrypt')
module.exports = class service {
  static async encriptPassword(password) {
    try {
      const salt = await bcrypt.genSalt(10)
      const hash = await bcrypt.hash(password, salt)
      return hash
    } catch (error) {
      return false
    }
  }

  static async comparePassword(password, oldPpassword) {
    try {
      return await bcrypt.compare(password, oldPpassword)
    } catch (error) {
      return false
    }
  }

  static createToken(user, time) {
    try {
      time = time || 60
      const token = jwt.sign(user, config.SECRET_KEY, {
        expiresIn: `${time}`,
        algorithm: 'HS512'
      })

      return token
    } catch (error) {
      return false
    }
  }


}
