const jwt = require('jsonwebtoken')
const config = require('../config/config')
const redis = require('../integrations/redis.connection')
module.exports = class globalHooks {
  static validateData(obj, keys) {
    try {
      if (!obj || !Array.isArray(keys)) return false

      const implementKeys = keys.reduce(
        (impl, key) => impl && key in obj,
        true
      )

      return implementKeys
    } catch (error) {
      return false
    }
  }

  static verifyEmailValidFormat(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }

  static async verifyToken(req, reply, next) {
    try {
      console.log(req, '--------------')
      const decoded = jwt.verify(req.headers.token, config.SECRET_KEY)
      console.log(decoded)

      const data = await redis.get(req.headers.token)

      if (decoded && data) {
        decoded.tk = req.headers.token
        decoded.data = JSON.parse(data)
        req.headers.token = decoded

        return
      }
      return reply.status(403).send({message: 'Invalid token', ok: false, http: 403})
    } catch (error) {
      return reply.status(403).send({message: 'Invalid token', ok: false, http: 403})
    }
  }

  static async decodeToken(token) {
    try {
      const data = jwt.decode(token)

      return data
    } catch (error) {
      return false
    }
  }



  static verifyTokenOtherServices(tk) {
    try {
      const decoded = jwt.verify(tk, config.SECRET_KEY)

      if (decoded) {
        return true
      }
      return false
    } catch (error) {
      return false
    }
  }

  static filterKeysService(object, keys) {
    Object.keys(object).forEach(function (key) {
      if (keys.indexOf(key) == -1) {
        delete object[key]
      }
    })
    return object
  }
}
