module.exports = class ErrorBasic {
  static basic(err) {
    try {
      const code = err.code ? err.code : ''
      const keyValue = err.keyValue ? err.keyValue : ''
      let result
      if (code && code == 11000) {
        result = {
          message: 'Duplicate value SQL Server Error',
          items: [keyValue],
          http: 409,
          ok: false
        }
      } else {
        result = {
          message: 'Internal Error',
          items: [],
          http: 500,
          ok: false
        }
      }

      return result
    } catch (error) {
      return {message: 'Internal Error', items: [], http: 500, ok: false}
    }
  }

  static error500(err) {
    return {message: 'Internal Error', items: [], http: 500, ok: false}
  }
}
