
import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private _snackBar: MatSnackBar) {}



  alert(msg: string, type: string) {
    return this._snackBar.open(msg, type, {
      horizontalPosition: 'right',
      verticalPosition: 'top',
      duration: 5 * 1000,
      panelClass: [type == 'Error' ? 'custom-style-alert' : 'custom-style-alert-success']
    });
  }


}
