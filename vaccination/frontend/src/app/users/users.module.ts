import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RoutinModule } from './users.route.module';
import { UserNewComponent } from './pages/new/component';
import { UserService } from './users.service';
import { UserListComponent } from './pages/list/component';
import { UserDeleteComponent } from './pages/delete/component';
import { ValidatorIdentityHook } from './pages/validator.identity.hook';
import { NgxMaskModule } from 'ngx-mask';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { UserProfileComponent } from './pages/profile/component';

@NgModule({
  declarations: [
    UserNewComponent,
    UserListComponent,
    UserProfileComponent,
    UserDeleteComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RoutinModule,
    NgxMaskModule.forRoot(),
    MaterialModule,
    Ng2SearchPipeModule,
    NgxDaterangepickerMd.forRoot(),
  ],
  providers: [UserService, ValidatorIdentityHook],
})
export class UserModule {}
