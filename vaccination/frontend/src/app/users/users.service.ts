import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IUser } from './interfaces';

@Injectable()
export class UserService {
  private apiVaccination: string = environment.apiVaccinationUrl;
  constructor(private http: HttpClient) {}

  createUser(obj: IUser) {
    return this.http.post(this.apiVaccination + 'user', obj).toPromise();
  }
  listUsers() {
    return this.http.get(this.apiVaccination + 'users').toPromise();
  }
  readUser(id: string) {
    return this.http.get(this.apiVaccination + 'user/' + id).toPromise();
  }

  searhUsers(query: string) {
    return this.http
      .get(this.apiVaccination + 'users/search?' + query)
      .toPromise();
  }

  editUser(id: string, obj: any) {

    return this.http.put(this.apiVaccination + 'user/' + id, obj).toPromise();
  }
  deleteUser(id: string) {
    return this.http.delete(this.apiVaccination + 'user/' + id).toPromise();
  }
  createVaccination(obj: any, id: string) {
    return this.http
      .post(this.apiVaccination + 'vaccination/' + id, obj)
      .toPromise();
  }
}
