import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Navigation, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { UserService } from '../../users.service';
import { ValidatorIdentityHook } from '../validator.identity.hook';

@Component({
  selector: 'app-user-profile',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class UserProfileComponent implements OnInit {
  name = '';
  vaccination_date = new Date();
  number_docess = 0;

  userForm: FormGroup = this.fb.group({
    date_of_brith_user: ['', [Validators.required]],
    address: ['', [Validators.required]],
    phone: ['', [Validators.required]],
    vaccination: [false],
  });

  status = ['Sputnik', 'AstraZeneca', 'Pfizer', 'Jhonson&Jhonson'];
  id: string = '';
  send: boolean = false;
  constructor(
    private route: Router,
    private fb: FormBuilder,
    private userService: UserService,
    private appService: AppService,
    private navigation: ActivatedRoute
  ) {}

  ngOnInit() {

    if (this.navigation.snapshot.params.id) {
      this.id = this.navigation.snapshot.params.id;
      this.userService.readUser(this.id).then((resp: any) => {

        if (resp && resp.items && resp.items[0]) {
          this.userForm.reset(resp.items[0]);
        }
      });
    }
  }

  saveUser() {

    this.userService
      .editUser(this.id, this.userForm.value)
      .then((resp) => {
        this.send = false;
        this.appService.alert('Usario Actulizado', 'Success');
      })
      .catch((error) => {
        this.appService.alert('Usario no actulizado', 'Error');
      });
  }

  saveVc() {
    const obj = {
      name: this.name,
      vaccination_date: this.vaccination_date,
      number_doses: this.number_docess,
    };
    this.userService.createVaccination(obj, this.id).then((resp) => {

    });
  }

  selectedStatus(e: any) {
    this.name = e.target.value;
  }
}
