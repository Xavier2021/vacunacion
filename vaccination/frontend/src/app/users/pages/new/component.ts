import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Navigation, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { UserService } from '../../users.service';
import { ValidatorIdentityHook } from '../validator.identity.hook';

@Component({
  selector: 'app-user-new',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class UserNewComponent implements OnInit {
  userForm: FormGroup = this.fb.group({
    name: ['', [Validators.required]],
    last_name: ['', [Validators.required]],
    ci: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
        validateIdentityFunc,
      ],
    ],
    email: ['', [Validators.required, Validators.email]],
    is_admin: [false, [Validators.required]],
  });
  id: string = '';
  customPattern = new RegExp('[a-zA-Z ]{2,254}');
  send: boolean = false;
  constructor(
    private route: Router,
    private fb: FormBuilder,
    private userService: UserService,
    private appService: AppService,
    private navigation: ActivatedRoute
  ) {}

  ngOnInit() {

    if (this.navigation.snapshot.params.id) {
      this.id = this.navigation.snapshot.params.id;
      this.userService.readUser(this.id).then((resp: any) => {

        if (resp && resp.items && resp.items[0]) {
          this.userForm.reset(resp.items[0]);
        }
      });
    }
  }

  userNew() {
    this.send = true;
    if (this.id) {
      return this.saveUser();
    }
    this.userService
      .createUser(this.userForm.value)
      .then((resp) => {

        this.appService.alert('Usuario nuevo creado exitosamente', 'Info');
        this.route.navigate(['users/list']);
        this.send = false;
      })
      .catch((err) => {

        this.appService.alert(
          'El numero de identificación, correo, name ya existe',
          'Error'
        );
        this.send = false;
      });
  }

  saveUser() {
    this.userService
      .editUser(this.id, this.userForm.value)
      .then((resp) => {
        this.send = false;
        this.appService.alert('Usario Actulizado', 'Success');
      })
      .catch((error) => {
        this.appService.alert('Usario no actulizado', 'Error');
      });
  }
  get email() {
    return this.userForm.get('email');
  }
  get ci() {
    return this.userForm.get('ci');
  }
}

function validateIdentityFunc(
  control: AbstractControl
): { [key: string]: any } | null {

  const validatorICI = new ValidatorIdentityHook();

  if (control.value && control.value.length === 10) {
    return validatorICI.ValidatorIdentityCI(control.value);
  }
  return null;
}
