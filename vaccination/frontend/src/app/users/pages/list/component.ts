import { Component, OnInit } from '@angular/core';
import { UserService } from '../../users.service';
import * as moment from 'moment/moment';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class UserListComponent implements OnInit {
  title = 'Admin list';
  listUsers: any;
  searhText: string = '';
  userId: string = '';
  confirm: boolean = false;
  ciUser: string = '';
  vaccination = [
    'Sputnik',
    'AstraZeneca',
    'Pfizer',
    'Jhonson&Jhonson',
    'Todos',
  ];
  status = ['Vacunados', 'No Vacunados', 'Todos'];
  selected = { startDate: null, endDate: null };
  selectedCi: string = '';
  constructor(
    private userService: UserService,
    private appService: AppService,
    private route: Router
  ) {}
  ngOnInit() {
    this.getListUsers();
    this.appService.alert(
      'El listado de usarios esta precargado con los ya vacunados cambie el status para ver otros!!!',
      'Info'
    );
  }
  selectedType(e: any) {
    e = e.target.value;
    e == 'Todos' ? this.getListUsers() : this.searchUsers(e);
  }
  selectedStatus(e: any) {
    e = e.target.value;

    this.filterStatus(e);
  }

  filterStatus(e: string) {
    this.userService.listUsers().then((resp: any) => {

      if (resp && resp.items) {
        this.listUsers = resp?.items;
        this.listUsers =
          e == 'Vacunados'
            ? (this.listUsers = this.listUsers.filter(
                (u: any) => u.vaccination_id && u.vaccination_id.length
              ))
            : e == 'No Vacunados'
            ? (this.listUsers = this.listUsers.filter(
                (u: any) => u.vaccination_id && !u.vaccination_id.length
              ))
            : this.getListUsers();
      }
    });
  }
  searchUsers(name?: string, from?: any, to?: any) {
    const query =
      from && name
        ? `name=${name}&from=${from}&to=${to}`
        : from && !name
        ? `from=${from}&to=${to}`
        : `name=${name}`;
    this.userService.searhUsers(query).then((resp: any) => {

      if (resp && resp.items) this.listUsers = resp?.items;
    });
  }
  getListUsers() {
    this.userService.listUsers().then((resp: any) => {

      if (resp && resp.items) this.listUsers = resp?.items;
    });
  }
  filterTime() {

    const from = moment(this.selected.startDate).format();
    const to = moment(this.selected.endDate).format();
    this.searchUsers(undefined, from, to);
  }
  editUser() {
    this.route.navigate(['users/update/' + this.userId]);
  }

  deleteUser() {
    this.confirm = true;
  }

  deleteConfirmation() {
    this.userService
      .deleteUser(this.userId)
      .then((resp: any) => {
        this.appService.alert('Usuario eliminado', 'Success');
        this.confirm = false;
        this.ngOnInit();
      })
      .catch((error) => {
        this.ngOnInit();
      });
  }
}
