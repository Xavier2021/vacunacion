import {Injectable} from "@angular/core";


@Injectable()
export class ValidatorIdentityHook {

  public ValidatorIdentityCI(ci: String) {
    let identity = false;
    let thirdDigit = parseInt(ci.substring(2, 3));
    if (thirdDigit < 6) {
      let coefficientValueCI = [2, 1, 2, 1, 2, 1, 2, 1, 2];
      let checker = parseInt(ci.substring(9, 10));
      let add: number = 0;
      let digit: number = 0;
      for (let i = 0; i < (ci.length - 1); i++) {
        digit = parseInt(ci.substring(i, i + 1)) * coefficientValueCI[i];
        add += ((parseInt((digit % 10) + '') + (parseInt((digit / 10) + ''))));

      }
      add = Math.round(add);
      identity = ((Math.round(add % 10) == 0) && (Math.round(add % 10) == checker)) ||
        ((10 - (Math.round(add % 10))) == checker) ? true : false;
    } else
      identity = false;



    return !identity ? {inValidCI: true} : null;
  }

}

