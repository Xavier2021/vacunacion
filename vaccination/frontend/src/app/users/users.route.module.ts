import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDeleteComponent } from './pages/delete/component';
import { UserListComponent } from './pages/list/component';
import { UserNewComponent } from './pages/new/component';
import { UserProfileComponent } from './pages/profile/component';

const routes: Routes = [
  {
    path: 'new',
    component: UserNewComponent,
  },
  {
    path: 'profile/:id',
    component: UserProfileComponent,
  },
  {
    path: 'update/:id',
    component: UserNewComponent,
  },
  {
    path: 'list',
    component: UserListComponent,
  },
  {
    path: 'delete',
    component: UserDeleteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutinModule {}
