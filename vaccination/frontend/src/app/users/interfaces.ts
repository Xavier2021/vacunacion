export interface IUser {
  ci: number;
  token?: string;
  name: string;
  last_name: string;
  email: string;
  address?: string;
  vaccination?: string;
  phone?: string;
  vaccination_id?: string;
  role_id?: string;
  is_admin?: boolean
}
